const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());


// MongoDB
const connectDB = require('./config/db');
connectDB();

//Puerto de la app
const PORT = process.env.PORT || 4000;

app.use(morgan('dev'));
app.use(cors());

// routes
app.use('/api/user/', require('./routes/auth.route'));
app.use('/api/category/', require('./routes/category.route'));
app.use('/api/product/', require('./routes/product.route'));

app.get('/', (req, res) => {
  res.send('test route => home page');
});

// Page Not founded
app.use((req, res) => {
  res.status(404).json({
    msg: 'Page not founded',
  });
});


app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}!`);
});

// Thanks for watching
